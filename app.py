import os

import dash
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
import plotly.graph_objs as go
from static import text

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
mapbox_access_token = 'pk.eyJ1IjoibWVncmV0c29uIiwiYSI6ImNqY3ptcTB2dTB4NmcyeXFvbjBpdmM0cmsifQ.2pZUUZ6yxxNTzIuPk4VKpg'

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
server = app.server

conemass = pd.read_csv('static/coneandseeddata.csv')
sample_sites = pd.read_csv('static/overviewsamplingsites.csv')
sample_sites["text"] = "Population: " + sample_sites["name"] + ", Mainland: " + sample_sites["mainland?"]


# Let's drop some columns we don't care about, and rename columns we'll use a lot
conemassavg = conemass.drop(['cone #'], axis=1)
conemassavg = conemassavg.groupby("population").mean()
conemassavg.reset_index(level=0, inplace=True)


hab2squir = pd.read_csv('static/chap15q23LodgepolePineCones.csv')

merged = pd.read_csv("static/merged.csv")
merged = merged.drop(['name', 'lat', 'long', 'symbols', 'Unnamed: 9', 'calls?'], axis=1)
merged = merged.rename(columns={'population': 'population', 'cone length': 'avg. cone length', 'cone width': 'avg. cone width',
                                'cone mass': 'avg. cone mass'})

acolumns = ["", "Df", "Sum Sq", "Mean Sq", "F value", "Pr(>F)", ""]
arow1 = ["habitat", "2", "29.404", "14.7020", "50.085", "7.87e-07", "***"]
arow2 = ["Residuals", "13", "3.816", "0.2935", "", "", ""]
arows = [arow1, arow2]

modelcol = ["", "Estimate", "Std. Error", "t value", "Pr(>|t|)", ""]
m1r1 = ["(Intercept)", "8.8979", "0.5817", "15.296", "9.52e-08", "***"]
m1r2 = ["mydata$squirrels", "-2.3353", "0.6821", "-3.424", "0.00758", "**"]
m1rows = [m1r1, m1r2]

m2r1 = ["(Intercept)",10.4492082,15.9683688,0.654,0.5338, ""]
m2r2 = ["squirrels.",  -2.2846788,  0.9784356,  -2.335,  0.0522, "."]
m2r3 = ["lat", -0.0338410,  0.1668666,  -0.203,   0.8451, ""]
m2r4 = ["long",-0.0005154,  0.1412423,  -0.004,   0.9972, ""]
m2rows = [m2r1, m2r2, m2r3, m2r4]

m3r1 = ["(Intercept)",   8.3808,     0.9466,   8.854, 2.09e-05, "***"]
m3r2 = ["squirrels.",   -2.0768,     0.7920,  -2.622,   0.0305, "*"]
m3r3 = ["bills.",        0.5171,     0.7332,   0.705,   0.5007, "" ]
m3rows = [m3r1, m3r2, m3r3]


def generate_map(dataframe):
    data = [
        go.Scattermapbox(
            lat=dataframe['lat'],
            lon=(-1 * dataframe['long']),
            mode='markers',
            marker=go.scattermapbox.Marker(
                size=9
            ),
            text=dataframe['text'],
        )
    ]

    layout = go.Layout(
        autosize=True,
        hovermode='closest',
        mapbox=go.layout.Mapbox(
            accesstoken=mapbox_access_token,
            bearing=0,
            center=go.layout.mapbox.Center(
                lat=dataframe['lat'].mean(),
                lon=-dataframe['long'].mean()
            ),
            pitch=0,
            zoom=2.5
        ),
    )

    return go.Figure(data=data, layout=layout)


def generate_table(dataframe, max_rows=10):
    return html.Table(
        # Header
        [html.Tr([html.Th(col) for col in dataframe.columns])] +

        # [f(x) if condition else g(x) for x in sequence]

        # Body
        [html.Tr([
            html.Td('%.3f' % dataframe.iloc[i][col]) if not isinstance(dataframe.iloc[i][col], str)
            else html.Td(dataframe.iloc[i][col]) for col in dataframe.columns
        ]) for i in range(min(len(dataframe), max_rows))]
    )

def generate_ANOVA(columns, rows):
    '''"",Df, Sum Sq, Mean Sq, F value, Pr(>F),""
habitat,2,29.404,14.7020,50.085,7.87e-07,***
Residuals,13,3.816,0.2935,,,'''

    table = html.Table(
        [html.Tr([html.Th(col) for col in columns])] +
        [html.Tr([html.Td(element) for element in row])for row in rows]
    )

    return table


app.layout = html.Div(children=[
    html.H1(children='Understanding Stats: the co-evolution of critters and cones '),

    html.H3(children="Quantitative Biology Final Project by Sarah Jo McGeady and Meg Anderson"),

    html.H2(children='Introduction'),

    dcc.Markdown(children=text.intro),

    html.Img(
        src='https://upload.wikimedia.org/wikipedia/commons/6/6e/Lodgepole_pine_Yellowstone_1998_near_firehole.jpg',
        alt="Image of Lodgepole Pines",
        style={'width': '500px'}),

    dcc.Markdown(children=text.intro2),

    html.Img(
        src='https://upload.wikimedia.org/wikipedia/commons/6/69/Charles%2C_King_of_the_Pine_Squirrels.JPG',
        alt="Image of Pine Squirrel",
        style={'width': '500px'}),

    html.Img(
        src='https://upload.wikimedia.org/wikipedia/commons/4/49/Red_Crossbills_%28Male%29.jpg',
        alt="Image of Crossbill birds",
        style={'width': '500px'}),

    dcc.Markdown(children=text.intro3),

    html.Img(
        src='https://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Pinus_contorta_subspecies_range_map_2.png/440px-Pinus_contorta_subspecies_range_map_2.png',
        alt="Image of Crossbill birds",
        style={'width': '500px'}),

    dcc.Markdown(children=text.intro4),

    html.Img(
        src='https://evolution.berkeley.edu/evolibrary/images/evo/pinecone_left.jpg',
        alt="Image of Crossbill-friendly pineccone",
        style={'width': '232px'}),

    html.Img(
        src='https://evolution.berkeley.edu/evolibrary/images/evo/pinecone_right.jpg',
        alt="Image of Squirrel-friendly pineccone",
        style={'width': '200px'}),

    html.H2(children="Building your first model"),

    html.H3(children='1: Asking Questions'),
    dcc.Markdown(children=text.askingq1),
    dcc.Markdown(children=text.askingq2),

    html.H3(children='2: Understanding the Data'),
    dcc.Markdown(children=text.understandd1),
    dcc.Markdown(children='You can read this csv into R and look at it using the following commands:'),
    dcc.Markdown(children="""
    pineConeData <- read.csv("C:/merged.csv")
    pineConeData
    
    """),
    dcc.Markdown(children='Let\'s take a look!'),

    html.H4(children='Population Features'),
    generate_table(merged, 20),

    html.Br(),
    html.H4(children='Collection Sites'),
    html.P(text.understandd2),
    dcc.Graph(id='graph', figure=generate_map(sample_sites)),

    html.H4(children='Habitat Type to Conemass'),
    html.P(text.understandd3),

    dcc.Markdown(children='You can make this graph in R using the following commands:'),
    dcc.Markdown(children="""
    squir <- read.csv("http://whitlockschluter.zoology.ubc.ca/wp-content/data/chapter15/chap15q23LodgepolePineCones.csv")
    stripchart(conemass ~ habitat, 
           data=squir, 
           method="jitter",
           vertical=TRUE,
           col="orange",
           pch=1 )
    """),

    dcc.Graph(
        id='habitat to conemass',
        figure={
            'data' : [
                go.Scatter(
                    x=hab2squir[hab2squir['habitat'] == i]['habitat'],
                    y=hab2squir[hab2squir['habitat'] == i]['conemass'],
                    text=hab2squir[hab2squir['habitat'] == i]['habitat'],
                    mode='markers',
                    opacity=0.7,
                    marker={
                        'size': 15,
                        'line': {'width': 0.5, 'color': 'white'}
                    },
                    name=i
                ) for i in hab2squir.habitat.unique()
            ],
            'layout': go.Layout(
                title={'text': 'Habitat Type to Conemass'},
                xaxis={'title': 'Habitat Type'},
                yaxis={'title': 'Conemass'},
                legend={'x': 1, 'y': 1},
                hovermode='closest'
            )
        }
    ),

    html.H5("Running an ANOVA: differences in means"),

    html.P(text.understandd4),
    html.P('You can run an ANOVA on this data using:'),
    dcc.Markdown(children="""
        squirAnova <- lm(conemass ~ habitat, data = squir)
        anova(squirAnova)
        squirAnovaSum <- summary(squirAnova)
    """),
    dcc.Markdown(children=text.understand5),

    generate_ANOVA(acolumns, arows),
    html.P("Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1"),

    html.Br(),
    dcc.Markdown(text.understand6),
    html.Br(),

    html.H5("Planned Comparison: Two sample t-test"),
    html.P('You can run a a two-sample t-test on just the island populations using:'),
    dcc.Markdown(children="""
    
    subset <- squir[1:11,]
    t.test(conemass ~ habitat, data=subset,
       var.equal=TRUE,
       conf.level=0.95)
    """),
    dcc.Markdown(text.understand7),
    dcc.Markdown(text.understand8),

    html.Br(),
    html.H3(children='3: Feature Selection and Linear Regression'),

    dcc.Markdown(text.featureselect1),

    html.H5(children='First model: conemass ~ squirrels'),
    dcc.Markdown(text.model1),
    dcc.Markdown(children= text.model1table),
    generate_ANOVA(modelcol, m1rows),
    dcc.Markdown(children= text.model1table2),

    html.H5(children='Second model: conemass ~ squirrels + lat + long'),
    dcc.Markdown(text.model2),
    html.Br(),
    dcc.Markdown(children=text.model1table),
    generate_ANOVA(modelcol, m2rows),
    dcc.Markdown(children=text.model2table2),

    html.H5(children='Third model: conemass ~ squirrels + bills'),
    dcc.Markdown(text.model3),
    html.Br(),
    dcc.Markdown(children=text.model1table),
    generate_ANOVA(modelcol, m3rows),
    dcc.Markdown(children=text.model3table2),

    html.Br()

])


if __name__ == '__main__':
    app.run_server(debug=True)
