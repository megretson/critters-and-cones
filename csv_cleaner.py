import pandas as pd

hab2squir = pd.read_csv('/Users/megretson/Desktop/Spring2019/QuantBio/FinalTake2/chap15q23LodgepolePineCones.csv')
samplsites = pd.read_csv('/Users/megretson/Desktop/Spring2019/QuantBio/FinalTake2/overviewsamplingsites.csv')
conemass = pd.read_csv('/Users/megretson/Desktop/Spring2019/QuantBio/FinalTake2/coneandseeddata.csv')

# Let's drop some columns we don't care about, and rename columns we'll use a lot
conemassavg = conemass.drop(['cone #'], axis=1)
conemassavg = conemassavg.groupby("population").mean()
conemassavg.reset_index(level=0, inplace=True)

conemassavg.to_csv("conemassavg.csv", encoding='utf-8', index=False)

print(hab2squir.head(n=4))

result = pd.merge(conemassavg, samplsites, how='inner', left_on=['population'], right_on=["name"])
# print(result)
result.to_csv("merged.csv", encoding='utf-8', index=False)
